import {Resend} from 'resend';
import {NextResponse} from 'next/server';
import {WelcomeEmail} from '@/emails';
import {connectToDB} from "@/db/database";
import User from "@/models/User";

const resend = new Resend(process.env.RESEND_API_KEY);

export async function POST(req: Request) {

    console.log('REQUEST', req)

    try {
        await connectToDB();

        const { formData } = await req.json();
        const { name, lastName, company, email, phone, message, contactPreference } = formData;
        

        const emailAlreadyExists = await User.findOne({email});
        const phoneAlreadyExists = await User.findOne({phone});

        if (emailAlreadyExists) {
            return NextResponse.json({
                status: 'error',
                message: 'Email already in use',
            });
        }

        if (phoneAlreadyExists) {
            return NextResponse.json({
                status: 'error',
                message: 'Phone number already in use',
            });
        }

        await User.create({
            name,
            lastName,
            company,
            email,
            phone,
            message,
            contactPreference
        });

        await resend.emails.send({
            from: 'pactum@pactumcompany.com',
            to: email,
            subject: `Seja-bem vindo(a) à Pactum, ${name}!`,
            react: WelcomeEmail({
                name,
                lastName,
                company,
                email
            }),
        });

        return NextResponse.json({
            status: 'ok',
            message: 'Email sent!'
        });

    } catch (error: any) {
        return NextResponse.json({
            status: 'error',
            message: error.message
        });
    }
}

module.exports = {
    POST
}