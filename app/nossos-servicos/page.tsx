'use client'

import {useState, useEffect} from 'react';
import ethLogo from "@/public/assets/ethereum-logo.svg";
import Image from "next/image";
import Form from "@/components/Form";
import block from "@/public/assets/3d-illustrations/block.png";
import whatsapp from "@/public/assets/icons/whatsapp.png";
import PrimaryButton from "@/components/PrimaryButton";
import {motion} from 'framer-motion';
import {cardVariants} from "@/utils/variants";
import emitter from "@/utils/events";


export default function ConhecaNossosServicos() {

    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        const handleFormSubmitted = (data: any) => {
            console.log("Form submitted:", data);
        };

        emitter.on('formSubmitted', handleFormSubmitted);
        
        return () => {
            emitter.off('formSubmitted', handleFormSubmitted);
        };
    }, []);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    return (
        <main
            className={`flex flex-col items-center justify-center w-full h-full bg-white gap-5 bg-white pb-10`}>

            <section className={'p-10 mt-32 gap-5 flex flex-col items-center w-full relative isolate'}>
                <div
                    className="absolute inset-x-0 -top-40 -z-10 transform-gpu overflow-hidden blur-3xl sm:-top-80"
                    aria-hidden="true"
                >
                    <div
                        className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"
                        style={{
                            clipPath:
                                'polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)',
                        }}
                    />
                </div>
                <h1 className={'text-5xl font-bold mt-10 text-black'}>Conheça nossos serviços</h1>

                <section className={'flex flex-wrap items-center text-center justify-center w-full h-full p-10 gap-5 servicos-container'}>

                    <motion.section
                        className={'text-black m-5 flex flex-col items-center text-center justify-center w-1/3 h-80 p-10 gap-5 bg-white rounded-2xl card shadow-2xl min-h-full servicos-card'}
                        variants={cardVariants}
                        initial={'initial'}
                        animate={'animate'}
                        exit={'exit'}
                        whileHover={'hover'}>
                        <h1 className={'text-black text-3xl font-bold mb-5'}>Desenvolvimento de contratos
                            inteligentes</h1>

                        <p>
                            Somos uma fábrica de software especializada em desenvolvimento de contratos inteligentes.
                        </p>
                        <p>
                            Transformamos contratos tradicionais em contratos digitais, que executam precisamente as
                            cláusulas
                            definidas nos exatos prazos estipulados.
                        </p>
                    </motion.section>

                    <motion.section
                        className={'text-black  m-5 flex flex-col items-center text-center justify-center w-1/3 h-80 p-10 gap-5 bg-white rounded-2xl card shadow-2xl min-h-full servicos-card'}
                        variants={cardVariants}
                        initial={'initial'}
                        animate={'animate'}
                        exit={'exit'}
                        whileHover={'hover'}
                    >
                        <h1 className={'text-black text-3xl font-bold mb-5'}>Desenvolvimento de blockchains privadas</h1>

                        <p>
                            Não deixe os dados do seu negócio dependentes de um banco de dados centralizado.
                            Utilize blockchains privadas para garantir a segurança e a integridade dos dados da sua
                            empresa.
                        </p>
                        <p>
                            As blockchains privadas oferecem segurança, traceabilidade e imutabilidade para os dados da
                            sua
                            empresa.
                        </p>
                    </motion.section>

                    <motion.section
                        className={'text-black m-5 flex flex-col items-center text-center justify-center w-1/3 h-80 p-10 gap-5 bg-white rounded-2xl card shadow-2xl min-h-full servicos-card'}
                        variants={cardVariants}
                        initial={'initial'}
                        animate={'animate'}
                        exit={'exit'}
                        whileHover={'hover'}>
                        <h1 className={'text-black text-3xl font-bold mb-5'}>Desenvolvimento de sites web3</h1>

                        <p>
                            Transformamos o seu site tradicional web2 para um site de fato moderno e que seja capaz
                            de interagir com diferentes blockchains e carteiras digitais.
                        </p>
                        <p>
                            Utilizamos o poder da web3 para criar soluções inovadoras e
                            personalizadas para cada cliente.
                        </p>
                    </motion.section>
                </section>

                {isOpen && <Form onClose={closeModal}/>}

                <h2 className={'text-4xl font-bold m-5 text-black'}>Ficou interessado?</h2>
                <div className={'w-1/3'}>
                    <PrimaryButton
                        text={'Fale com um de nossos especialistas!'}
                        onClick={openModal}
                    />
                </div>
            </section>
            <a href={'https://api.whatsapp.com/send?phone=5561996386998&text=Ol%C3%A1!%20Quero%20saber%20mais%20sobre%20os%20servi%C3%A7os%20da%20Pactum!'}
               target={'_blank'}>
                <Image src={whatsapp} alt={'Whatsapp icon'}
                       className={'fixed bottom-5 right-5 animate-pulse'}
                       width={75} height={75}/>
            </a>
        </main>
    );
}