'use client'

import {useState} from 'react';
// import errorMountain from "@/public/assets/not-found-mountain.svg";
import Image from "next/image";

export default function NotFound() {


    return (
        <main className={`flex flex-col items-center justify-center w-full h-full p-10 bg-white`}>
            <h1 className={'text-2xl'}>Ops... alguma coisa deu errado</h1>
            <Image src={'../../public/assets/not-found-mountaing.svg'} alt={'Ether logo'} width={500} height={500}/>
        </main>
    )
}