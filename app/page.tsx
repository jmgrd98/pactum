'use client'

import {useState} from "react";
import Form from "@/components/Form";
import 'animate.css'
import Hero from '../components/Hero';


export default function Home() {
    const [isOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    return (
        <main className={'min-h-full'}>
            <Hero/>
            {isOpen && <Form onClose={closeModal}/>}
        </main>

    )
}
