'use client'

import {useState} from 'react';
import realEstate from "@/public/assets/illustrations/For sale-cuate.svg";
import Image from "next/image";
import Accordion from "@/components/Accordion";
import {motion} from "framer-motion";
import Form from "@/components/Form";
import PrimaryButton from "@/components/PrimaryButton";
import whatsapp from "@/public/assets/icons/whatsapp.png";

export default function ParaImobiliarias() {

    const [isOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    return (
        <main className={`flex flex-col items-center justify-center w-full h-full p-10 gap-5 bg-white`}>
            <section className={'p-10 mt-32 flex flex-col items-center gap-5 w-full relative isolate'}>
                <div
                    className="absolute inset-x-0 -top-40 -z-10 transform-gpu overflow-hidden blur-3xl sm:-top-80"
                    aria-hidden="true"
                >
                    <div
                        className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"
                        style={{
                            clipPath:
                                'polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)',
                        }}
                    />
                </div>
                <h1 className={'text-black text-5xl font-bold'}>Soluções em contratos inteligentes para
                    imobiliárias.</h1>

                <p className={'text-black text-xl text-center'}>Mergulhe no futuro da corretagem de imóveis com contratos
                    inteligentes.
                    Automatize processos, reduza erros humanos e garanta a execução impecável de cláusulas
                    contratuais.</p>

                <Image src={realEstate} alt={'Ether logo'} width={500} height={500}/>

                <div className={'flex flex-col gap-5'}>
                <h2 className={'text-black text-xl'}>Perguntas frequentes de corretores sobre contratos
                    inteligentes</h2>
                <Accordion/>
                </div>


                <div className={'w-1/2 flex flex-col gap-5'}>
                    <h2 className={'text-black text-xl text-center'}>Ainda tem alguma dúvida?</h2>

                    <PrimaryButton
                        text={'Entrar em contato'}
                        onClick={openModal}
                    />
                </div>
            </section>

            <a href={'https://api.whatsapp.com/send?phone=5561996386998&text=Ol%C3%A1!%20Quero%20saber%20mais%20sobre%20os%20servi%C3%A7os%20da%20Pactum!'}
               target={'_blank'}>
                <Image src={whatsapp} alt={'Whatsapp icon'}
                       className={'fixed bottom-5 right-5 animate-pulse'}
                       width={75} height={75}/>
            </a>
            {isOpen && <Form onClose={closeModal}/>}
        </main>
    )
}