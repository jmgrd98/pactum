'use client'

import {useState} from 'react';
import Image from "next/image";
import {createHash} from "crypto";
import whatsapp from "@/public/assets/icons/whatsapp.png";
import Form from "@/components/Form";
import {toast, ToastContainer} from "react-toastify";
import {motion} from 'framer-motion';
import PrimaryButton from "@/components/PrimaryButton";
import AlertButton from "@/components/AlertButton";
import BlockCard from "@/components/BlockCard";
import {AiOutlineArrowDown} from 'react-icons/ai';

function generateHash(data: string): string {
    const hash = createHash('sha256');
    hash.update(data);
    return hash.digest('hex');
}

class Block {
    number: number;
    date: Date;
    data: string;
    hash: string;
    prev_hash: string | null;

    constructor(number: number, hash: string, data: string, prev_hash: string | null) {
        this.number = number;
        this.date = new Date();
        this.data = data;
        this.hash = hash;
        this.prev_hash = prev_hash;
    }
}

class BlockChain {
    chain: Block[];

    constructor() {
        this.chain = [];
        this.genesis_block();
    }

    genesis_block(): void {
        const genesis = new Block(0, generateHash("Genesis"), "Genesis", null);
        this.chain.push(genesis);
    }

    add_block(data: string): void {
        const prev_hash = this.chain[this.chain.length - 1].hash;
        const hash = generateHash(data + prev_hash);
        const block = new Block(this.chain.length, hash, data, prev_hash);
        this.chain.push(block);
    }

}

export default function SimuleUmaBlockchain() {
    const [blockchain, setBlockchain] = useState(new BlockChain());
    const [blockData, setBlockData] = useState('');

    const handleAddBlock = () => {

        if (blockData === '') {
            toast.warn('O conteúdo do bloco não pode ser vazio!');
            return;
        }

        const newBlockchain = new BlockChain();
        newBlockchain.chain = [...blockchain.chain];
        newBlockchain.add_block(blockData);
        setBlockchain(newBlockchain);
        setBlockData('');
    };

    function cleanBlockchain() {
        setBlockchain(new BlockChain());

    }

    const [isOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    return (
        <main className={`text-black flex flex-col items-center justify-center w-full min-h-screen bg-white`}>

            <section className={'p-10 mt-40 flex flex-col items-center w-full relative isolate'}>
                <div
                    className="absolute inset-x-0 -top-40 -z-10 transform-gpu overflow-hidden blur-3xl sm:-top-80"
                    aria-hidden="true"
                >
                    <div
                        className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"
                        style={{
                            clipPath:
                                'polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)',
                        }}
                    />
                </div>
                <h1 className={'text-black text-5xl font-bold'}>Simule uma blockchain</h1>
                <p className={'text-xl m-3'}>Entenda como funciona uma blockhain!</p>


                <div className="mt-5 relative">
                    {blockchain.chain.map((block, index) => (
                        <motion.div
                            key={index}
                            initial={{ opacity: 0, scale: 0.5 }}
                            animate={{ opacity: 1, scale: 1 }}
                            exit={{ opacity: 0, scale: 0.5 }}
                            transition={{ duration: 0.2 }}
                            className="block-container"
                        >
                            <BlockCard
                                number={block.number}
                                date={block.date}
                                data={block.data}
                                hash={block.hash}
                                prev_hash={block.prev_hash}
                            />
                            {index !== blockchain.chain.length - 1 && <AiOutlineArrowDown className="link text-center justify-items-center justify-center self-center m-5 w-full h-10 text-purple-500"/>}
                        </motion.div>
                    ))}
                </div>


                <div className={'w-1/2 p-8'}>
                    <input
                        type="text"
                        value={blockData}
                        onChange={(e) => setBlockData(e.target.value)}
                        placeholder="Digite o conteúdo do bloco..."
                        className="block w-full mb-5 rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                    <div className={'flex gap-3 '}>
                        <PrimaryButton
                            text={'Adicionar bloco'}
                            onClick={handleAddBlock}
                        />
                        <AlertButton
                            text={'Reiniciar blockchain'}
                            onClick={cleanBlockchain}
                        />
                    </div>
                </div>
            </section>

            <a href={'https://api.whatsapp.com/send?phone=5561996386998&text=Ol%C3%A1!%20Quero%20saber%20mais%20sobre%20os%20servi%C3%A7os%20da%20Pactum!'}
               target={'_blank'}>
                <Image src={whatsapp} alt={'Whatsapp icon'}
                       className={'fixed bottom-5 right-5 animate-pulse'}
                       width={75} height={75}/>
            </a>

            {isOpen && <Form onClose={closeModal}/>}

            <style jsx>{`

              .link {
                align-self: center;
                justify-self: center;
                width: 2px;
                height: 20px;
                background-color: black;
              }
            `}</style>

            <ToastContainer
                position="bottom-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick={true}
                rtl={false}
                pauseOnFocusLoss={false}
                draggable={true}
                pauseOnHover={true}
            />
        </main>
    );
}
