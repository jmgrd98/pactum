'use client'

import {useState} from 'react';
import Form from "@/components/Form";
import 'animate.css';
import PrimaryButton from "@/components/PrimaryButton";

export default function SobreNos() {

    const [isOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    return (
        <main className={`flex flex-col items-center justify-center w-full h-full p-10 gap-10 bg-white`}>
            <section className={'p-10 mt-40 flex flex-col items-center w-full relative isolate'}>
                <div
                    className="absolute inset-x-0 -top-40 -z-10 transform-gpu overflow-hidden blur-3xl sm:-top-80"
                    aria-hidden="true"
                >
                    <div
                        className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"
                        style={{
                            clipPath:
                                'polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)',
                        }}
                    />
                </div>
                <h1 className={`text-5xl font-bold self-start animate__animated animate__fadeInUp mb-20`}>Quem é a
                    Pactum?</h1>
                <p className={`text-2xl font-semibold text-gray-700 self-start w-1/2 animate__animated animate__fadeInUp`}>Somos
                    especialistas em desenvolvimento de contratos inteligentes utilizando a tecnologia
                    blockchain para ajudar escritórios de advocacia, contabilidade e firmas imobiliárias a melhorar o
                    seus negócios e a serem mais competitivas em seus mercados.</p>
                <p className={`text-2xl font-semibold self-end w-1/2 animate__animated animate__fadeInUp`}>Nossa missão
                    é ajudar empresas a migrarem os seus contratos tradicionais para contratos inteligentes e assim
                    se tornarem mais eficientes e lucrativas através de alta tecnologia
                    blockchain.</p>
                <p className={`text-2xl font-semibold text-gray-700 self-start w-1/2 animate__animated animate__fadeInUp`}>Nossa
                    visão é ser a empresa líder em desenvolvimento de contratos inteligentes no Brasil.</p>
                <p className={`text-2xl font-semibold self-end w-1/2 animate__animated animate__fadeInUp`}>Nossos
                    valores são: ética, transparência, inovação, qualidade e comprometimento.</p>

                <div className={'mt-20 w-1/3'}>
                    <PrimaryButton
                        text={'Entrar em contato'}
                        onClick={openModal}
                    />
                </div>

            </section>

            {isOpen && <Form onClose={closeModal}/>}
        </main>
    )
}