'use client'

import { Disclosure, Transition } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/20/solid'

export default function Accordion() {
    return (
        <div className="w-full px-4">
            <div className="mx-auto w-full max-w-md rounded-2xl bg-white p-2">
                <Disclosure>
                    {({ open }) => (
                        <>
                            <Disclosure.Button
                                className="flex w-full justify-between rounded-lg bg-purple-100 px-4 py-2 text-left text-sm font-medium text-purple-900 hover:bg-purple-200 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75"
                            >
                                <span>O que são contratos inteligentes?</span>
                                <ChevronDownIcon
                                    className={`${
                                        open ? 'rotate-180 transform' : ''
                                    } h-5 w-5 text-purple-500`}
                                />
                            </Disclosure.Button>
                            <Transition
                                show={open}
                                enter="transition duration-100 ease-out"
                                enterFrom="transform scale-95 opacity-0"
                                enterTo="transform scale-100 opacity-100"
                                leave="transition duration-75 ease-out"
                                leaveFrom="transform scale-100 opacity-100"
                                leaveTo="transform scale-95 opacity-0"
                            >
                                {(ref) => (
                                    <Disclosure.Panel
                                        ref={ref}
                                        className="px-4 pt-4 pb-2 text-sm text-gray-500"
                                    >
                                        Os contratos inteligentes são programas de computador autônomos que executam automaticamente os termos de um contrato quando as condições predefinidas são atendidas. Eles são executados em uma blockchain e são imutáveis e transparentes.
                                    </Disclosure.Panel>
                                )}
                            </Transition>
                        </>
                    )}
                </Disclosure>
                <Disclosure as="div" className="mt-2">
                    {({ open }) => (
                        <>
                            <Disclosure.Button
                                className="flex w-full justify-between rounded-lg bg-purple-100 px-4 py-2 text-left text-sm font-medium text-purple-900 hover:bg-purple-200 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75"
                            >
                                <span>Os contratos inteligentes são legalmente vinculativos?</span>
                                <ChevronDownIcon
                                    className={`${
                                        open ? 'rotate-180 transform' : ''
                                    } h-5 w-5 text-purple-500`}
                                />
                            </Disclosure.Button>
                            <Transition
                                show={open}
                                enter="transition duration-100 ease-out"
                                enterFrom="transform scale-95 opacity-0"
                                enterTo="transform scale-100 opacity-100"
                                leave="transition duration-75 ease-out"
                                leaveFrom="transform scale-100 opacity-100"
                                leaveTo="transform scale-95 opacity-0"
                            >
                                {(ref) => (
                                    <Disclosure.Panel
                                        ref={ref}
                                        className="px-4 pt-4 pb-2 text-sm text-gray-500"
                                    >
                                        Sim, os contratos inteligentes podem ser legalmente vinculativos, desde que atendam aos requisitos legais para um contrato tradicional. Isso inclui a existência de consentimento mútuo, consideração, capacidade legal e objetivos lícitos.
                                    </Disclosure.Panel>
                                )}
                            </Transition>
                        </>
                    )}
                </Disclosure>
                <Disclosure as="div" className="mt-2">
                    {({ open }) => (
                        <>
                            <Disclosure.Button
                                className="flex w-full justify-between rounded-lg bg-purple-100 px-4 py-2 text-left text-sm font-medium text-purple-900 hover:bg-purple-200 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75"
                            >
                                <span>Quais são os principais benefícios dos contratos inteligentes?</span>
                                <ChevronDownIcon
                                    className={`${
                                        open ? 'rotate-180 transform' : ''
                                    } h-5 w-5 text-purple-500`}
                                />
                            </Disclosure.Button>
                            <Transition
                                show={open}
                                enter="transition duration-100 ease-out"
                                enterFrom="transform scale-95 opacity-0"
                                enterTo="transform scale-100 opacity-100"
                                leave="transition duration-75 ease-out"
                                leaveFrom="transform scale-100 opacity-100"
                                leaveTo="transform scale-95 opacity-0"
                            >
                                {(ref) => (
                                    <Disclosure.Panel
                                        ref={ref}
                                        className="px-4 pt-4 pb-2 text-sm text-gray-500"
                                    >
                                        Os benefícios incluem a automação de processos, redução de custos, eliminação de intermediários, transparência, segurança e execução precisa dos termos do contrato.
                                    </Disclosure.Panel>
                                )}
                            </Transition>
                        </>
                    )}
                </Disclosure>
                <Disclosure as="div" className="mt-2">
                    {({ open }) => (
                        <>
                            <Disclosure.Button
                                className="flex w-full justify-between rounded-lg bg-purple-100 px-4 py-2 text-left text-sm font-medium text-purple-900 hover:bg-purple-200 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75"
                            >
                                <span>Quais são os custos envolvidos na implementação de contratos inteligentes?</span>
                                <ChevronDownIcon
                                    className={`${
                                        open ? 'rotate-180 transform' : ''
                                    } h-5 w-5 text-purple-500`}
                                />
                            </Disclosure.Button>
                            <Transition
                                show={open}
                                enter="transition duration-100 ease-out"
                                enterFrom="transform scale-95 opacity-0"
                                enterTo="transform scale-100 opacity-100"
                                leave="transition duration-75 ease-out"
                                leaveFrom="transform scale-100 opacity-100"
                                leaveTo="transform scale-95 opacity-0"
                            >
                                {(ref) => (
                                    <Disclosure.Panel
                                        ref={ref}
                                        className="px-4 pt-4 pb-2 text-sm text-gray-500"
                                    >
                                        Os custos incluem desenvolvimento de software, auditoria de segurança, taxas de blockchain e manutenção contínua. Os custos podem variar dependendo da complexidade do contrato.
                                    </Disclosure.Panel>
                                )}
                            </Transition>
                        </>
                    )}
                </Disclosure>
                <Disclosure as="div" className="mt-2">
                    {({ open }) => (
                        <>
                            <Disclosure.Button
                                className="flex w-full justify-between rounded-lg bg-purple-100 px-4 py-2 text-left text-sm font-medium text-purple-900 hover:bg-purple-200 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75"
                            >
                                <span>Quais são os principais benefícios dos contratos inteligentes?</span>
                                <ChevronDownIcon
                                    className={`${
                                        open ? 'rotate-180 transform' : ''
                                    } h-5 w-5 text-purple-500`}
                                />
                            </Disclosure.Button>
                            <Transition
                                show={open}
                                enter="transition duration-100 ease-out"
                                enterFrom="transform scale-95 opacity-0"
                                enterTo="transform scale-100 opacity-100"
                                leave="transition duration-75 ease-out"
                                leaveFrom="transform scale-100 opacity-100"
                                leaveTo="transform scale-95 opacity-0"
                            >
                                {(ref) => (
                                    <Disclosure.Panel
                                        ref={ref}
                                        className="px-4 pt-4 pb-2 text-sm text-gray-500"
                                    >
                                        Automação de tarefas rotineiras, eliminação de intermediários, transparência e prevenção de disputas, redução de erros humanos, eficiência na gestão de documentos, execução precisa de cláusulas entre muitos outros.
                                    </Disclosure.Panel>
                                )}
                            </Transition>
                        </>
                    )}
                </Disclosure>
            </div>
        </div>
    )
}
