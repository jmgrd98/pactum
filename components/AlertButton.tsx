'use client'

export default function AlertButton({text, onClick}: any) {


    return(
        <button
            onClick={onClick}
            className="rounded-md w-full bg-yellow-500 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-[#ffbe0b] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
        >
            {text}
        </button>
    )
}