'use client'


export default function BlockCard({number, date, data, hash, prev_hash, ...props}: any) {


    return (
        <>
            <div className="p-10 bg-white shadow shadow-2xl border rounded rounded-2xl flex flex-col gap-3">
                <div className={'flex gap-2 items-center'}>
                    <p className={'font-bold text-lg'}>
                        Bloco #{number || '0'}
                    </p>
                    <p>{date.toLocaleString()}</p>
                </div>
                <p className={'text-md'}>Conteúdo: {data}</p>
                <div className={'flex gap-2 items-center '}>
                    <p>Hash:</p>
                    <p className={'text-sm text-purple-700 bg-purple-200 border-2 border-purple-700 rounded p-1'}>{hash}</p>
                </div>
                <p>Hash do bloco anterior: {prev_hash || '0'}</p>
            </div>
        </>

    )
}