'use client'
import {toast, ToastContainer} from "react-toastify";
import Input from "@/components/Input";
import { useFormContext } from '@/context/FormContext';
import Loader from './Loader';

export default function Contact() {

    const { formData, setFormData, handleSubmit, loading } = useFormContext();

    const handleChange = (name: string, value: string) => {
        setFormData((prevFormData: any) => ({
            ...prevFormData,
            [name]: value
        }));
    };

    return (
        <div className="text-black relative px-6 py-5  lg:px-8" id={'contact'}>
            <div
                className="absolute inset-x-0 -z-10 transform-gpu overflow-hidden blur-3xl sm:-top-80"
                aria-hidden="true"
            >
                <div
                    className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"
                    style={{
                        clipPath:
                            'polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)',
                    }}
                />
            </div>
            <div className="mx-auto max-w-2xl text-center">
                <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Entre em contato
                    conosco.</h2>
                <p className="mt-2 text-lg leading-8 text-gray-600">
                    Retornaremos assim que possível.
                </p>
            </div>
            <form onSubmit={handleSubmit} method="POST" className="mx-auto mt-5 max-w-xl">
                <div className="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
                    <div>
                        <label htmlFor="first-name" className="block text-sm font-semibold leading-6 text-gray-900">
                            Nome
                        </label>
                        <div className="mt-2.5">
                            <Input
                                name={'name'}
                                value={formData.name}
                                onInputChange={(e: any) => handleChange('name', e)}
                                required={true}
                            />
                        </div>
                    </div>
                    <div>
                        <label htmlFor="last-name" className="block text-sm font-semibold leading-6 text-gray-900">
                            Sobrenome
                        </label>
                        <div className="mt-2.5">
                            <Input
                                name={'lastName'}
                                value={formData.lastName}
                                onInputChange={(e: any) => handleChange('lastName', e)}
                                required={true}
                            />
                        </div>
                    </div>
                    <div className="sm:col-span-2">
                        <label htmlFor="company" className="block text-sm font-semibold leading-6 text-gray-900">
                            Empresa
                        </label>
                        <div className="mt-2.5">
                            <Input
                                name={'company'}
                                value={formData.company}
                                onInputChange={(e: any) => handleChange('company', e)}
                                required={true}
                            />
                        </div>
                    </div>
                    <div className="sm:col-span-2">
                        <label htmlFor="email" className="block text-sm font-semibold leading-6 text-gray-900">
                            Email
                        </label>
                        <div className="mt-2.5">
                            <Input
                                name={'email'}
                                value={formData.email}
                                onInputChange={(e: any) => handleChange('email', e)}
                                required={true}
                            />
                        </div>
                    </div>
                    <div className="sm:col-span-2">
                        <label htmlFor="phone-number" className="block text-sm font-semibold leading-6 text-gray-900">
                            Telefone
                        </label>
                        <div className="relative mt-2.5">
                            <Input
                                name={'phone'}
                                value={formData.phone}
                                onInputChange={(e: any) => handleChange('phone', e)}
                                required={true}
                            />
                        </div>
                    </div>
                    <div className="sm:col-span-2">
                        <label htmlFor="message" className="block text-sm font-semibold leading-6 text-gray-900">
                            Mensagem
                        </label>
                        <div className="mt-2.5">
                            <Input
                                name={'message'}
                                value={formData.message}
                                onInputChange={(e: any) => handleChange('message', e)}
                            />
                        </div>
                    </div>
                </div>
                <div className={'flex gap-3 mt-2'}>
                    <div className={'flex gap-2 items-center'}>
                        <label>Email</label>
                        <input
                            type={'radio'}
                            name={'contactPreference'}
                            value={'email'}
                            checked={formData.contactPreference === 'email'}
                            onChange={(e: any) => handleChange('contactPreference', 'email')}
                        />
                    </div>
                    <div className={'flex gap-2 items-center'}>
                        <label>Whatsapp</label>
                        <input
                           type={'radio'}
                           name={'contactPreference'}
                           value={'whatsapp'}
                           checked={formData.contactPreference === 'whatsapp'}
                           onChange={(e: any) => handleChange('contactPreference', 'whatsapp')}
                        />
                    </div>
                </div>
                <div className="mt-10 w-full text-center">
                <button
                    type="submit"
                    className="block w-full rounded-md bg-indigo-600 px-3.5 py-2.5 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                >
                    {loading ? 'Enviando...' : 'Enviar'}
                </button>
                </div>
            </form>
            <ToastContainer
                position="bottom-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick={true}
                rtl={false}
                pauseOnFocusLoss={false}
                draggable={true}
                pauseOnHover={true}
                className={'z-0'}
            />
        </div>
    )
}