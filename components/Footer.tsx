'use client'

export default function Footer() {

    return (
        <footer className="flex items-center w-full p-5 z-0 bg-white">

            <p >Copyright 2023 - All right reserved.</p>

        </footer>

    )
}