'use client'

import {useState} from 'react'
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Input from "../components/Input";
import {GrClose} from 'react-icons/gr'
import { useFormContext } from '@/context/FormContext';

export default function Form({onClose}: { onClose: () => void }) {

    const { formData, setFormData, handleSubmit, loading } = useFormContext();

    const handleChange = (name: string, value: string) => {
        setFormData((prevFormData: any) => ({
            ...prevFormData,
            [name]: value
        }));
    };

    function handleOverlayClick(e: any) {
        if (e.target.classList.contains('modal-overlay')) {
            onClose();
        }
    }


    return (
        <div className={'flex flex-col align-items justify-center'}>
            <div
                className="fixed inset-0 bg-black bg-opacity-50 z-10 flex items-center justify-center modal-overlay"
                onClick={handleOverlayClick}
            ></div>
            <form onSubmit={handleSubmit} method="POST" className="max-h-[500px] overflow-y-scroll text-black fixed z-50 flex flex-col gap-3 p-10 items-center rounded bg-white w-1/2 top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
            <div className={'flex justify-between items-center w-full'}>
                    <h1 className={'text-2xl font-bold text-center'}>Entre em contato</h1>
                    <button onClick={onClose}>
                        <GrClose className={'text-2xl'}/>
                    </button>
                </div>

                <p className={'mt-5 mb-5 text-xl text-center'}>Preencha esse formulário e nós retornaremos o mais rápido
                    possível!</p>
                <div className="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
                    <div>
                        <label htmlFor="first-name" className="block text-sm font-semibold leading-6 text-gray-900">
                            Nome
                        </label>
                        <div className="mt-2.5">
                            <Input
                                name={'name'}
                                value={formData.name}
                                onInputChange={(e: any) => handleChange('name', e)}
                                required={true}
                            />
                        </div>
                    </div>
                    <div>
                        <label htmlFor="last-name" className="block text-sm font-semibold leading-6 text-gray-900">
                            Sobrenome
                        </label>
                        <div className="mt-2.5">
                            <Input
                                name={'lastName'}
                                value={formData.lastName}
                                onInputChange={(e: any) => handleChange('lastName', e)}
                                required={true}
                            />
                        </div>
                    </div>
                    <div className="sm:col-span-2">
                        <label htmlFor="company" className="block text-sm font-semibold leading-6 text-gray-900">
                            Empresa
                        </label>
                        <div className="mt-2.5">
                            <Input
                                name={'company'}
                                value={formData.company}
                                onInputChange={(e: any) => handleChange('company', e)}
                                required={true}
                            />
                        </div>
                    </div>
                    <div className="sm:col-span-2">
                        <label htmlFor="email" className="block text-sm font-semibold leading-6 text-gray-900">
                            Email
                        </label>
                        <div className="mt-2.5">
                            <Input
                                name={'email'}
                                value={formData.email}
                                onInputChange={(e: any) => handleChange('email', e)}
                                required={true}
                            />
                        </div>
                    </div>
                    <div className="sm:col-span-2">
                        <label htmlFor="phone-number" className="block text-sm font-semibold leading-6 text-gray-900">
                            Telefone
                        </label>
                        <div className="relative mt-2.5">
                            <Input
                                name={'phone'}
                                value={formData.phone}
                                onInputChange={(e: any) => handleChange('phone', e)}
                                required={true}
                            />
                        </div>
                    </div>
                    <div className="sm:col-span-2">
                        <label htmlFor="message" className="block text-sm font-semibold leading-6 text-gray-900">
                            Mensagem
                        </label>
                        <div className="mt-2.5">
                            <Input
                                value={formData.message}
                                onInputChange={(e: any) => handleChange('message', e)}
                                name="message"
                            />
                        </div>
                    </div>
                </div>
                <div className={'flex gap-3 mt-2'}>
                    <div className={'flex gap-2 items-center'}>
                        <label>Email</label>
                        <input
                            type={'radio'}
                            name={'contactPreference'}
                            value={'email'}
                            checked={formData.contactPreference === 'email'}
                            onChange={() => handleChange('contactPreference', 'email')}
                        />
                    </div>
                    <div className={'flex gap-2 items-center'}>
                        <label>Whatsapp</label>
                        <input
                            type={'radio'}
                            name={'contactPreference'}
                            value={'whatsapp'}
                            checked={formData.contactPreference === 'whatsapp'}
                            onChange={() => handleChange('contactPreference', 'whatsapp')}
                        />
                    </div>
                </div>
                <div className="mt-10">
                    <button
                        type="submit"
                        className="block w-full rounded-md bg-indigo-600 px-3.5 py-2.5 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                    >
                        {loading ? 'Enviando...' : 'Enviar'}
                    </button>
                </div>
            </form>

            <ToastContainer
                position="bottom-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick={true}
                rtl={false}
                pauseOnFocusLoss={false}
                draggable={true}
                pauseOnHover={true}
            />
        </div>
    )
}