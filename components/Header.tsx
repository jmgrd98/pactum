'use client'

import Link from 'next/link';
import Image from 'next/image';
import logo from '../public/assets/pactum-black-logo.svg';
import {useState, useEffect} from 'react';
import Form from "@/components/Form";
import {toast, ToastContainer} from "react-toastify";
import Dropdown from "@/components/Dropdown";
import {Bars3Icon, XMarkIcon} from "@heroicons/react/24/outline";
import {Dialog} from "@headlessui/react";
import { useFormContext } from '@/context/FormContext';
import { useParams } from 'next/navigation';

const navigation = [
    {name: 'Simule uma blockchain', href: '/simule-uma-blockchain'},
    {name: 'Serviços', href: '/nossos-servicos'},
    // {name: 'Sobre nós', href: '/sobre-nos'},
]

export default function Header() {
    const router = useParams();
    const { formData, setFormData, handleSubmit, loading } = useFormContext();
    const currentPath = router.pathname;
    const { pathname, query, asPath } = router;
    const isHomePage = currentPath === '/' || currentPath === '/index';

    const [mobileMenuOpen, setMobileMenuOpen] = useState(false)
    const [isOpen, setIsOpen] = useState(false);


    return (
        <>
            <header className="absolute inset-x-0 px-5 sm:p-5 z-50">
                <nav className="flex items-center justify-between" aria-label="Global">
                    <div className="flex lg:flex-1">
                        <Link href="/" className="-m-1.5 p-1.5">
                            <Image src={logo} alt={'Pactum Company'} width={200} />
                        </Link>
                    </div>
                    <div className="flex lg:hidden">
                        <button
                            type="button"
                            className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
                            onClick={() => setMobileMenuOpen(true)}
                        >
                            <span className="sr-only">Open main menu</span>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>
                    </div>
                    <div className="hidden lg:flex lg:gap-x-12">
                        {navigation.map((item) => (
                            <Link key={item.name} href={item.href} className="text-sm font-semibold leading-6 text-gray-900">
                                {item.name}
                            </Link>
                        ))}
                    </div>
                    <div className="hidden lg:flex lg:flex-1 lg:gap-2 lg:justify-end lg:align-items">
                        <Dropdown/>
                        <button
                            onClick={(e) => {
                                e.preventDefault();
                                console.log(isHomePage)
                                if (isHomePage) {
                                    const contactSection = document.getElementById('contact');
                                    if (contactSection) {
                                        window.scrollTo({
                                            top: contactSection.offsetTop,
                                            behavior: 'smooth',
                                        });
                                    }
                                }
                               setIsOpen(true);
                            }}
                            className="rounded-md bg-indigo-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                        >
                            Entrar em contato
                        </button>
                    </div>
                </nav>
                <Dialog as="div" className="lg:hidden" open={mobileMenuOpen} onClose={setMobileMenuOpen}>
                    <div className="fixed inset-0 z-50" />
                    <Dialog.Panel className="fixed inset-y-0 right-0 z-50 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
                        <div className="flex items-center justify-between">
                            <Link href="/" className="-m-1.5 p-1.5">
                                <span className="sr-only">Pactum Company</span>
                                <Image src={logo} alt={'Pactum'} width={200}/>
                            </Link>
                            <button
                                type="button"
                                className="-m-2.5 rounded-md p-2.5 text-gray-700"
                                onClick={() => setMobileMenuOpen(false)}
                            >
                                <span className="sr-only">Close menu</span>
                                <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                            </button>
                        </div>
                        <div className="mt-6 flow-root">
                            <div className="-my-6 divide-y divide-gray-500/10">
                                <div className="space-y-2 py-6">
                                    {navigation.map((item) => (
                                        <Link
                                            key={item.name}
                                            href={item.href}
                                            className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                                        >
                                            {item.name}
                                        </Link>
                                    ))}
                                </div>
                                <div className="py-6">
                                    <button
                                        onClick={(e) => {
                                            e.preventDefault();
                                            const contactSection = document.getElementById('contact');
                                            if (contactSection) {
                                                window.scrollTo({
                                                    top: contactSection.offsetTop,
                                                    behavior: 'smooth',
                                                });
                                            }
                                        }}
                                        className="rounded-md bg-indigo-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                    >
                                        Entrar em contato
                                    </button>
                                </div>
                            </div>
                        </div>
                    </Dialog.Panel>
                </Dialog>
            </header>
            {isOpen && <Form onClose={() => setIsOpen(false)}/>}

        </>

    )
}