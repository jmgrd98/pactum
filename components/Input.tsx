import React, {useState} from 'react';

export default function Input({name, type, value, placeholder, onInputChange, disabled, ...props}: any) {

    const [inputValue, setInputValue] = useState('');

    const handleChange = (event: any) => {
        const newValue = event.target.value;
        onInputChange(newValue, name);
    };

    return (
        <>
            <input
                className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                type={type}
                placeholder={placeholder}
                name={name}
                onChange={handleChange}
                value={value}
                disabled={disabled}
            />
        </>
    )
}