// Loader.js

import React from 'react';

interface LoaderProps {
  width: number;
  height: number;
}

const Loader: React.FC<LoaderProps> = ({ width, height }) => {
  return (
    <div className="flex justify-center items-center">
      <div className={`w-${width} h-${height} border-t-4 border-b-4 border-purple-600 rounded-full animate-spin`} />
    </div>
  );
};

export default Loader;
