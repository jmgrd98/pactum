// 'use client'
//
// import React, {
//     useRef,
//     useEffect,
//     forwardRef,
// } from "react";
// import {
//     ViewerApp,
//     AssetManagerPlugin,
//     GBufferPlugin,
//     ProgressivePlugin,
//     TonemapPlugin,
//     SSRPlugin,
//     SSAOPlugin,
//     BloomPlugin,
//     GammaCorrectionPlugin,
// } from "webgi";
// import {ethAnimation} from "@/lib/ethAnimation";
//
// const WebgiViewer = forwardRef((props, ref) => {
//     const canvasRef = useRef(null);
//
//     const setupViewer = async () => {
//         try {
//             const viewer = new ViewerApp({
//                 canvas: canvasRef.current,
//             });
//
//             const manager = await viewer.addPlugin(AssetManagerPlugin);
//
//             await viewer.addPlugin(GBufferPlugin);
//             await viewer.addPlugin(new ProgressivePlugin(32));
//             await viewer.addPlugin(new TonemapPlugin(true));
//             await viewer.addPlugin(GammaCorrectionPlugin);
//             await viewer.addPlugin(SSRPlugin);
//             await viewer.addPlugin(SSAOPlugin);
//             await viewer.addPlugin(BloomPlugin);
//
//             viewer.renderer.refreshPipeline();
//
//             const ethLogo = await manager.addFromPath("ethereum_logo.glb");
//             if (ethLogo) {
//                 ethAnimation(ethLogo);
//             }
//
//             viewer.getPlugin(TonemapPlugin).config.clipBackground = true;
//             viewer.scene.activeCamera.setCameraOptions({ controlsEnabled: false });
//             window.scrollTo(0, 0);
//         } catch (error) {
//             console.error("Error setting up the viewer:", error);
//         }
//     };
//
//     useEffect(() => {
//         setupViewer();
//     }, []);
//
//     return (
//         <div id='webgi-canvas-container'>
//             <canvas id='webgi-canvas' ref={canvasRef} />
//         </div>
//     )
// });
//
// WebgiViewer.displayName = 'WebgiViewer';
// export default WebgiViewer;
