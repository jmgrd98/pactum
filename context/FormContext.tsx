'use client'

import { createContext, useState, useContext } from 'react';
import { toast } from 'react-toastify';

type FormData = {
  name: string;
  lastName: string;
  company: string;
  email: string;
  phone: string;
  message: string;
  contactPreference: string;
};

type FormDataContextType = {
  formData: FormData;
  setFormData: React.Dispatch<React.SetStateAction<FormData>>;
  handleSubmit: (e: React.FormEvent<HTMLFormElement>) => Promise<void>;
  loading: boolean;
};

const FormContext = createContext<FormDataContextType | null>(null);

export const FormProvider = ({ children }: any) => {
  const [formData, setFormData] = useState<FormData>({
    name: '',
    lastName: '',
    company: '',
    email: '',
    phone: '',
    message: '',
    contactPreference: '',
  });
  const [loading, setLoading] = useState(false);
  const handleSubmit = async (e: any) => {
    e.preventDefault();

    if (!formData.name || !formData.lastName || !formData.company || !formData.email || !formData.phone || !formData.message || !formData.contactPreference) {
        toast.warn('Por favor, preencha todos os campos!')
        return
    }

    if (!formData.email.includes('@')) {
        toast.warn('E-mail inválido!')
        return
    }

    if (formData.phone.length < 11) {
        toast.warn('Telefone inválido!')
        return
    }

    setLoading(true);
    try {
      const response = await sendDataToBackend();

      if (response.status === 'error') {
          if (response.message.includes('Email')) {
              toast.error('Esse e-mail já está sendo utilizado!');
          }
          if (response.message.includes('Phone')) {
              toast.error('Esse telefone já está sendo utilizado!');
          }
      } else {
          toast.success('Formulário enviado com sucesso! Cheque o seu e-mail cadastrado. 😉');
          setFormData({
              name: '',
              lastName: '',
              company: '',
              email: '',
              phone: '',
              message: '',
              contactPreference: '',
          });
      }
    } catch (error) {
        console.error(error);
        toast.error('Erro ao enviar o formulário.');
    } finally {
      setLoading(false);
  }
    
};

const sendDataToBackend = async () => {
    try {
        const response = await fetch('/api/email', {
            method: 'POST',
            body: JSON.stringify({ formData }),
            headers: {
                'Content-Type': 'application/json',
            },
        });

        return await response.json();
    } catch (error) {
        console.error(error);
        return { status: 'error', message: 'Erro ao enviar o formulário.' };
    }
};


  return (
    <FormContext.Provider value={{ formData, setFormData, handleSubmit, loading }}>
      {children}
    </FormContext.Provider>
  );
};

export const useFormContext = () => {
  const context = useContext(FormContext);
  if (!context) {
    throw new Error('useFormContext must be used within a FormProvider');
  }
  return context;
};
