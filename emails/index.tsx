import {
    Body,
    Button,
    Container,
    Head,
    Heading,
    Hr,
    Html,
    Preview,
    Section,
    Text,
} from '@react-email/components';
import * as React from 'react';

import {Img} from '@react-email/img'
import logo from '../static/pactum.png';
interface WelcomeEmailProps {
    name: string;
    lastName: string;
    company: string;
    email: string;
}

const baseUrl = process.env.VERCEL_URL
    ? `https://${process.env.VERCEL_URL}`
    : '';

export const WelcomeEmail = ({name, lastName, email, company}:WelcomeEmailProps) => (
    <Html>
        <Head />
        <Heading>Seja bem-vindo(a)!</Heading>
        <Body style={main}>
            {/*<Img src="../static/pactum.png" alt="Logo da Pactum" width={'42'} height={'42'} />*/}
            <Container style={container}>
                <Text style={paragraph}>Olá {name} {lastName},</Text>
                <Text style={paragraph}>
                    Obrigado por entrar em contato com a Pactum!
                </Text>
                <Text style={paragraph}>
                    Em breve entraremos em contato com você para que possamos impactar a {company} através dos contratos inteligentes!
                </Text>
                <Text style={paragraph}>
                    Atenciosamente,
                    <br />
                    Pactum
                </Text>
                <Hr style={hr} />
            </Container>
        </Body>
    </Html>
);

export default WelcomeEmail;

const main = {
    backgroundColor: '#ffffff',
    fontFamily:
        '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif',
};

const container = {
    margin: '0 auto',
    padding: '20px 0 48px',
};


const paragraph = {
    fontSize: '16px',
    lineHeight: '26px',
};

const btnContainer = {
    textAlign: 'center' as const,
};

const button = {
    backgroundColor: '#5F51E8',
    borderRadius: '3px',
    color: '#fff',
    fontSize: '16px',
    textDecoration: 'none',
    textAlign: 'center' as const,
    display: 'block',
};

const hr = {
    borderColor: '#cccccc',
    margin: '20px 0',
};

const footer = {
    color: '#8898aa',
    fontSize: '12px',
};