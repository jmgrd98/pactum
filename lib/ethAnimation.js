import gsap from "gsap";

export const ethAnimation = (object) => {
    if (!object) return;

    gsap.to(object.rotation, {
        duration: 5, // Duration of the animation in seconds
        y: "+=360", // Rotate 360 degrees on the Y axis
        repeat: -1, // Infinite loop
        ease: "linear" // Smooth, constant rotation
    });
}