import {Schema, model, models} from "mongoose";

const UserSchema = new Schema({
    name: {
        type: String,
        required: [true, "Name is required"],
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"],
    },
    company: {
        type: String,
        required: [true, "Company is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"],
        unique: [true, "Email already exists"],
    },
    phone: {
        type: String,
        required: [true, "Phone is required"],
        unique: [true, "Phone already exists"],
    },
    message: {
        type: String,
        required: [true, "Message is required"]
    },
    contactPreference: {
        type: String,
        required: [true, "Contact preference is required"],
        enum: ["email", "whatsapp"],
    }
});


const User = models.User || model("User", UserSchema);

export default User;