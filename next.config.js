/** @type {import('next').NextConfig} */
const nextConfig = {
    experimental: {
        appDir: true,
        serverComponentsExternalPackages: ["mongoose"],
    },
    env: {
        RESEND_API_KEY: 're_aybjnRPA_NUM9XXCLhGxcDH4h2wndkCAc',
        MONGODB_URI: "mongodb+srv://jmgrd98:ilovecode98@apicluster0.y7vkluv.mongodb.net/?retryWrites=true&w=majority&appName=APICluster0",
        VERCEL_URL: 'https://www.pactumcompany.com/'
    },
    images: {
        domains: ['lh3.googleusercontent.com'],
    },
    webpack(config) {
        config.experiments = {
            ...config.experiments,
            topLevelAwait: true,
        }
        return config
    }
}

module.exports = nextConfig
