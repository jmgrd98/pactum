const containerVariants = {
    initial: {
        opacity: 0,
        x: '100vw'
    },
    animate: {
        opacity: 1,
        x: 0,
        transition: {
            duration: 2.5,
            type: 'spring',
        }
    },
    exit: {
        x: '-100vw',
        transition: {
            ease: 'easeInOut'
        }
    }
}

const h1Variants = {
    initial: {
        opacity: 0,
        x: '-100vw',
    },
    animate: {
        opacity: 1,
        x: 0,
        transition: {
            type: 'spring',
            duration: 2.5,
        }
    },
}

const linkVariants = {
    initial: {
        y: -65,
        opacity: 0,
    },
    animate: {
        y: 0,
        opacity: 1,
        transition: {
            delay: 0.5,
            duration: 1,
            type: "spring",
        }
    },
    hover: {
        scale: 1.1,
        marginLeft: 1.1,
        marginRight: 1.1,
        textShadow: '0px 0px 10px white',
    }
}

const buttonVariants = {
    initial: {
        y: -65,
        opacity: 0,
    },
    animate: {
        y: 0,
        opacity: 1,
        transition: {
            delay: 0.5,
            duration: 1,
            type: "spring",
        }
    },
    hover: {
        scale: 1.1,
        marginLeft: 1.1,
        marginRight: 1.1,
        boxShadow: '0px 0px 10px 0px white',
    }
}

const cardVariants = {
    initial: {
        boxShadow: '0px 0px 0px 0px rgba(0,0,0,0)',
        opacity: 0,
        scale: 1
    },
    animate: {
        boxShadow: '0px 0px 10px 0px rgba(0,0,0,0.2)',
        scale: 1,
        opacity: 1,
        transition: {
            type: 'spring',
            duration: .5,
            delay: .5,
        }
    },
    exit: {
        opacity: 0,
        scale: 0,
        transition: {
            type: 'spring',
            duration: .5,
        }
    },
    hover: {
        scale: 1.1,
    }
}

const iconVariants = {
    initial: {
        opacity: 0,
        y: 50,

    },
    animate: {
        y: 0,
        opacity: 1,
        transition: {
            type: 'spring',
            duration: .5,
            delay: 1
        }
    },
    hover: {
        scale: 1.1,
    }
}

export { containerVariants, h1Variants, linkVariants, buttonVariants, cardVariants, iconVariants };